# Simple Wagtail in Docker example

This is an example repo demonstrating running a [Wagtail](http://wagtail.io) site inside a Docker container.

Interesting things to note are the [Dockerfile](https://gitlab.com/aaronmarruk/simple-wagtail-docker-example/blob/master/Dockerfile "Dockerfile"). This is used to describe the Docker container which can be built from this repo.

There's also a [start.sh](https://gitlab.com/aaronmarruk/simple-wagtail-docker-example/blob/master/start.sh) file which is called inside the container and starts the Wagtail server, running on Gunicorn.

A full blog post running through setting up this project is coming soon. If you have any questions please [open an issue](https://gitlab.com/aaronmarruk/simple-wagtail-docker-example/issues/new) or catch me on [Twitter](http://twitter.com/aaronmarruk)